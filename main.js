// @ts-nocheck
"use strict";

const addTodoButton = document.querySelector(".add-todo-button");
const todoInput = document.querySelector("#add-todo-input");
const todosDiv = document.querySelector(".todos");

// Where are using event delegation here. We attached listener to parent element of todo checkboxes
todosDiv.addEventListener("change", function (e) {
  if (e.target.checked) {
    updateCompleted(e.target.id, true);
  } else {
    updateCompleted(e.target.id, false);
  }
});

// Saatı insan başa düşə biləcək formata çevirmək üçün funksiya
const getDateNow = () => {
  // return (
  //   new Date().toLocaleDateString().split("T")[0] +
  //   " " +
  //   new Date().toLocaleDateString().split("T")[1].split(".")[0]
  // );
  return (
    new Date().toLocaleDateString() +
    " " +
    new Date().toLocaleTimeString("az-Latn-AZ", {
      hour12: false,
    })
  );
};

// İlkin olaraq səhifədə göstəriləcək todo siyahısı
const todos = [
  {
    title: "JS kurslarına baxmaq",
    status: {
      done: false,
      when: getDateNow(),
    },
    date: getDateNow(),
  },
  {
    title: "JS tapşırıqları yerinə yetirmək",
    status: {
      done: false,
      when: getDateNow(),
    },
    date: getDateNow(),
  },
];

// Add ToDo düyməsinə basıldıqda nə baş versin
addTodoButton?.addEventListener("click", () => {
  // Əlavə edilən ToDo sətrindən boşluqları silək
  const todoTitle = todoInput.value.trim();
  if (!todoTitle) {
    alert("ToDo sətri boş ola bilməz!");
    todoInput.value = "";
    return;
  }
  // atodos massivinə əlavə etmək üçün yeni obyekt hazırlamaq
  const obj = {
    title: todoTitle,
    status: {
      done: false,
      when: getDateNow(),
    },
    date: getDateNow(),
  };
  // input box-un içini silmək üçün
  todoInput.value = "";
  // yeni todo-nu atodos massivinin ilk elementi kimi əlavə etmək
  todos.unshift(obj);
  // Hər yeni əlavədən sonra saytda görsənən siyahını yeniləmək
  loadTodos();
  // console.log(`Yeni tapşırıq əlavə edildi: ${txt}`);
});

// Massivin içində verilmiş index-də olan obyektin bitib, bitmədiyini dəyişmək
const updateCompleted = (index, isCompleted) => {
  todos[index].status.done = isCompleted;
  todos[index].status.when = getDateNow();
  loadTodos();
};

// Yeni Todo elementini saytda göstərmək üçün html blok hazırla və geri qaytar
const makeTodoItemBlock = (id, spanTxt, isCompleted) => {
  let todoBlockDiv = document.createElement("div");
  todoBlockDiv.classList.add("todo");
  let todoBlockChk = document.createElement("input");
  todoBlockChk.type = "checkbox";
  todoBlockChk.id = id;
  todoBlockChk.checked = isCompleted;
  let todoBlockSpan = document.createElement("span");
  if (isCompleted) {
    todoBlockSpan.style.fontStyle = "italic";
  }
  todoBlockSpan.textContent = spanTxt;
  todoBlockDiv.appendChild(todoBlockChk);
  todoBlockDiv.appendChild(todoBlockSpan);
  return todoBlockDiv;
};

const loadTodos = () => {
  // Köhnə siyahını saytdan sil
  todosDiv.innerHTML = "";
  // Massivi parçala və siyahı kimi göstər
  for (let i = 0; i < todos.length; i++) {
    let finishDateTime = todos[i].status.when;
    let startDateTime = todos[i].date;
    let lastingSeconds = Date.parse(finishDateTime) - Date.parse(startDateTime);

    const finishedTxt = ` bitib:  ${finishDateTime} (${lastingSeconds.msToTime()})`;
    // Siyahıda görsənəcək sətrin human readable versiyası
    let todoItem = `${todos[i].title} |  
    ${todos[i].status.done ? finishedTxt : "bitməyib"} | ${startDateTime}`;
    // Yeni todo item blok yarat və siyahıya əlavə et
    let todoHTML = makeTodoItemBlock(i, todoItem, todos[i].status.done);
    todosDiv.appendChild(todoHTML);
  }
};
// Show milliseconds in Hours:Minutes:Seconds format
// Usage: let lastingSeconds=600; console.log(lastingSeconds.msToTime())
Number.prototype.msToTime = function (duration) {
  let milliseconds = Math.floor((this % 1000) / 100),
    seconds = Math.floor((this / 1000) % 60),
    minutes = Math.floor((this / (1000 * 60)) % 60),
    hours = Math.floor((this / (1000 * 60 * 60)) % 24),
    days = Math.floor(((this / (1000 * 60 * 60)) % 24) % 24);

  hours = hours < 10 ? "0" + hours : hours;
  minutes = minutes < 10 ? "0" + minutes : minutes;
  seconds = seconds < 10 ? "0" + seconds : seconds;

  return days + " gün " + hours + ":" + minutes + ":" + seconds;
};

// Səhifə açılanda köhnə siyahını saytda göstər
loadTodos();
